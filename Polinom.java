package tema1_tp;
import java.util.ArrayList;

public class Polinom {
	private ArrayList<Monom> list;
	
	
	public Polinom()
	{
		this.list=new ArrayList<>();
	}
	
	public void addMonom( Monom m)
	{
		this.list.add(m);
	
	}
	
	public void afisarePolinom()
	{
		for(Monom m:this.list)
			System.out.print(m.getCoeficient()+"x^"+m.getGrad()+" ");
		System.out.println();
	}
	
	public ArrayList<Monom> getList()
	{
		return this.list;
	}
	
	public void verificarePolinom ()
	{	
		for(int i=0;i<this.list.size();i++)
			for(int j=i+1;j<this.list.size();j++)
			{
				if(this.list.get(i).getGrad()==this.list.get(j).getGrad())
				{
					this.list.get(i).setCoeficient(this.list.get(i).getCoeficient()+this.list.get(j).getCoeficient());
					this.list.remove(this.list.get(j));
					
				}
			}
	}
	
	public String afisarePolinomS()
	{
		String rezultat = "";
		for(Monom m:this.getList())
			
			rezultat=rezultat+ Float.toString(m.getCoeficient())+"x^"+Integer.toString(m.getGrad())+"+";
		return rezultat;
	}

	
}
