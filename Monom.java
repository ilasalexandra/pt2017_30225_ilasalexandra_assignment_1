package tema1_tp;

public class Monom {
	private Integer grad;
	private Float coeficient;
	
	
	public Integer getGrad() 
	{
		return grad;
	}
	
	public void setGrad(Integer a)
	{
		this.grad=a;
	}
	
	public Float getCoeficient() 
	{
		return coeficient;
	}
	public void setCoeficient(Float a)
	{
		this.coeficient=a;
	}
	public Monom(Float a, Integer b)
	{
	  this.coeficient=a;
	  this.grad=b;
	  
	}

}
