package tema1_tp;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GUI extends JPanel {

	public static void main(String[] args) 
	{ 
		JFrame frame = new JFrame ("Calculator Polinoame");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE ); 
		frame.setSize(640,480);
		JPanel panel1 = new JPanel(); 
		JPanel panel2 = new JPanel();
		JPanel panel3= new JPanel();
		
		JLabel l1 = new JLabel ("Polinom1 ");
		JLabel l2 = new JLabel ("Polinom2 ");
		JTextField tf1 = new JTextField(" ");
		JTextField tf2 = new JTextField(" ");

		panel1.add(l1); 
		panel1.add(tf1);
		panel1.add(l2);
		panel1.add(tf2);
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
		//panel1.setAlignmentX(0);
	    //panel2.setLayout(new FlowLayout());
		JButton b1 = new JButton("Adunare");
		JButton b2 = new JButton("Scadere");
		JButton b3 = new JButton("Inmultire");
		JButton b4 = new JButton("Derivare");
		JButton b5 = new JButton("Integrare");
		
		panel2.add(b1);
		panel2.add(b2);
		panel2.add(b3);
		panel2.add(b4);
		panel2.add(b5);
		panel2.setLayout(new BoxLayout(panel2 , BoxLayout . Y_AXIS));
		
		JLabel l3 = new JLabel ("Rezultat ");
		JTextField tf3 = new JTextField(" ");
		panel3.add(l3);
		panel3.add(tf3);
		panel3.setLayout(new BoxLayout(panel3 , BoxLayout . Y_AXIS));
		
		JPanel p = new JPanel();
		p.add(panel1); 
		//p.add(panel2);  
		p.add(panel2);
		p.add(panel3);
		p.setLayout(new BoxLayout(p , BoxLayout . X_AXIS));
		
		frame.setContentPane(p);
		frame.setVisible(true); 
		
		
		//adunare
		b1.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	Operatii o=new Operatii();
		    	Polinom p1=new Polinom();
		    	Polinom p2=new Polinom();
		    	
		    	p1=o.convertToPolinom(tf1.getText());
		    	p2=o.convertToPolinom(tf2.getText());
			
				tf3.setText(o.adunarePolinoame(p1,p2).afisarePolinomS());
				
		    }
		});
		
		//scadere
		b2.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	Operatii o=new Operatii();
		    	Polinom p1=new Polinom();
		    	Polinom p2=new Polinom();
		    	
		    	p1=o.convertToPolinom(tf1.getText());
		    	p2=o.convertToPolinom(tf2.getText());
			
		    	tf3.setText(o.scaderePolinoame(p1,p2).afisarePolinomS());
				
		    }
		});
		
		//inmultire
		b3.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	Operatii o=new Operatii();
		    	Polinom p1=new Polinom();
		    	Polinom p2=new Polinom();
		    	
		    	p1=o.convertToPolinom(tf1.getText());
		    	p2=o.convertToPolinom(tf2.getText());
			
		    	tf3.setText(o.inmultirePolinoame(p1,p2).afisarePolinomS());
				
		    }
		});
		
		//derivare
		b4.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	Operatii o=new Operatii();
		    	Polinom p1=new Polinom();
		    
		    	p1=o.convertToPolinom(tf1.getText());
		 
			
		    	tf3.setText(o.derivarePolinom(p1).afisarePolinomS());
				
		    }
		});
		
		//integrare
		b5.addActionListener( new ActionListener()
		{
		    public void actionPerformed(ActionEvent e)
		    {
		    	Operatii o=new Operatii();
		    	Polinom p1=new Polinom();
		    	
		    	
		    	p1=o.convertToPolinom(tf1.getText());
		    	
			
		    	tf3.setText(o.integrarePolinom(p1).afisarePolinomS());
				
		    }
		});
		
		
	}
	

}
