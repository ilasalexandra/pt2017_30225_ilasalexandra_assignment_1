package tema1_tp;

public class Operatii {
	
	
	public  Polinom adunarePolinoame(Polinom p,Polinom q)
	{
		Polinom rez= new Polinom();
		for(Monom m: p.getList() )
			for(Monom n:q.getList())
			{
				if(m.getGrad()>n.getGrad())
				{
					rez.addMonom(m);
					break;
				}
				if(m.getGrad()==n.getGrad())
				{   
					Monom m1=new Monom(m.getCoeficient()+n.getCoeficient(),m.getGrad());
					rez.addMonom(m1);
					break;
				}
				
			}
		//rez.verificarePolinom();
		rez.afisarePolinom();
		return rez;
	}
	
	public Polinom scaderePolinoame(Polinom p,Polinom q)
	{
		Polinom rez=new Polinom();
		for(Monom m:p.getList())
			for(Monom n:q.getList())
			{
				if(m.getGrad()>n.getGrad())
				{
					rez.addMonom(m);
					break;
				}
				
				if(m.getGrad()==n.getGrad())
				{
					Monom m1=new Monom(m.getCoeficient()-n.getCoeficient(),m.getGrad());
					rez.addMonom(m1);
					break;
				}
				
			
			}
		rez.afisarePolinom();
		return rez;
	}
	
	public Polinom inmultirePolinoame(Polinom p,Polinom q)
	{
		Polinom rez=new Polinom();
		for(Monom m:p.getList())
			for(Monom n:q.getList())
			{
				Monom m1=new Monom(m.getCoeficient()*n.getCoeficient(),m.getGrad()+n.getGrad());
				rez.addMonom(m1);
			}
		
		
		rez.verificarePolinom();
		rez.afisarePolinom();
		return rez;
	}

	public Polinom derivarePolinom(Polinom p)
	{
		Polinom rez= new Polinom();
		for(Monom m:p.getList())
		{
			if(m.getGrad()!=0)
			{
				Monom m1=new Monom (m.getCoeficient()*m.getGrad(),m.getGrad()-1);
				rez.addMonom(m1);
			}
			
				
			
		}
		rez.afisarePolinom();
		return rez;
			
	}
	
	public Polinom integrarePolinom(Polinom p)
	{
		Polinom rez=new Polinom();
		for(Monom m:p.getList())
		{
			Monom m1=new Monom(m.getCoeficient()/(m.getGrad()+1),m.getGrad()+1);
			rez.addMonom(m1);
		}
		rez.afisarePolinom();
		return rez;
	}
	
	public Polinom convertToPolinom(String s)
	{
		Polinom p=new Polinom();
		String[]x=s.split("[+]");
		
		for(String c:x)
		{
			

			c=c.replace(" ", "");
			String[] x1=c.split("\\^");
			
			x1[0]=x1[0].replace("x", "");
			Monom m=new Monom(Float.parseFloat(x1[0]),Integer.parseInt(x1[1]));
			p.addMonom(m);
		}
		return p;
	}
	

	//1x^3+2x^2+3x^0
}
